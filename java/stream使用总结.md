### 中间操作

---

#### 无状态:

- 过滤操作: 

  - filter

    ```java
    List<String> list = Arrays.asList("A", "B", "C", "A");
    List<String> result = list.stream().filter("A"::equals).collect(Collectors.toList());
    System.out.println(result); // ["A", "A"]
    ```

- 映射操作: 

  - map: 

    ```java
    List<User> list = Arrays.asList(new User("张三", 10), new User("李四", 11), new User("王五", 12), new User("赵六", 13));
    List<Integer> result = list.stream().map(User::getAge).collect(Collectors.toList());
    System.out.println(result); // [10, 11, 12, 13]
    ```

  - flatMap: 

    ```java
    List<List<User>> list = Arrays.asList(Collections.singletonList(new User("张三", 10)), Collections.singletonList(new User("李四", 11)), Collections.singletonList(new User("王五", 12)), Collections.singletonList(new User("赵六", 13)));
    List<User> result = list.stream().flatMap(Collection::stream).collect(Collectors.toList());
    System.out.println(result); // [{"name":"张三","age":10},{"name":"李四","age":11},{"name":"王五","age":12},{"name":"赵六","age":13}]
    ```

- 窥视操作:

  - peek:

    ```java
    List<List<User>> list = Arrays.asList(Collections.singletonList(new User("张三", 10)), Collections.singletonList(new User("李四", 11)), Collections.singletonList(new User("王五", 12)), Collections.singletonList(new User("赵六", 13)));
    List<User> result = list.stream().flatMap(Collection::stream).peek(item -> item.setAge(110)).collect(Collectors.toList());
    System.out.println(JsonUtils.objectToJson(result)); // [{"name":"张三","age":110},{"name":"李四","age":110},{"name":"王五","age":110},{"name":"赵六","age":110}]
    ```



#### 有状态:

- 排序操作:

  - sorted:

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    List<Integer> result = list.stream().sorted().collect(Collectors.toList());
    System.out.println(JsonUtils.objectToJson(result)); // [2,3,5,7,8]
    ```

  - sorted(Comparator): 可以指定比较器, 例如倒序

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    List<Integer> result = list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    System.out.println(JsonUtils.objectToJson(result)); // [8,7,5,3,2]
    ```

- 数量操作:

  - limit: 按顺序获取指定数量

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    List<Integer> result = list.stream().limit(2).collect(Collectors.toList());
    System.out.println(JsonUtils.objectToJson(result)); // [5,3]
    ```

  - skip: 按顺序跳过指定数量

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    List<Integer> result = list.stream().skip(2).collect(Collectors.toList());
    System.out.println(JsonUtils.objectToJson(result)); // [7,2,8]
    ```



### 终端操作:

---

#### 非短路:

- 遍历操作:

  - forEach:

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    list.forEach(System.out::println); // 5, 3, 7, 2, 8
    ```

  - forEachOrdered: 并行操作时按顺序遍历

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    list.parallelStream().forEach(System.out::println); // 7, 8, 3, 5, 2 顺序不确定
    list.parallelStream().forEachOrdered(System.out::println); // 5, 3, 7, 2, 8
    ```

- 收集操作:

  - toArray:

    ```java
    List<Integer> list = Arrays.asList(5, 3, 7, 2, 8);
    Object[] objects = list.stream().sorted().toArray();
    ```

  - reduce: 

    ```java
    // 累计
    List<BigDecimal> list = Arrays.asList(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
    System.out.println(list.stream().reduce(BigDecimal::add).orElse(BigDecimal.ZERO)); // 5
    ```

    ```java
    // 求最大值
    List<BigDecimal> list = Arrays.asList(BigDecimal.ONE, BigDecimal.valueOf(2), BigDecimal.valueOf(12), BigDecimal.valueOf(53), BigDecimal.valueOf(8));
    System.out.println(list.stream().reduce(BigDecimal::max).orElse(BigDecimal.ZERO)); // 53
    ```

    ```java
    // 处理逻辑
    List<BigDecimal> list = Arrays.asList(BigDecimal.ONE, BigDecimal.valueOf(2), BigDecimal.valueOf(12), BigDecimal.valueOf(53), BigDecimal.valueOf(8));
    System.out.println(list.stream().reduce((var1, var2) -> var1.compareTo(var2) > 0 ? var1 : var2).orElse(BigDecimal.ZERO)); // 53
    ```

  - collect:

    ```java
    // 转list, set同理
    Integer[] arr = {1, 2, 3, 4};
    List<Integer> result = Arrays.stream(arr).collect(Collectors.toList());
    ```

    ```java
    // 转map
    List<User> list = Arrays.asList(new User("张三", 10), new User("李四", 11), new User("王五", 12), new User("赵六", 13));
    Map<String, Integer> result = list.stream().collect(Collectors.toMap(User::getName, User::getAge));
    System.out.println(JsonUtils.objectToJson(result)); // {"李四":11,"张三":10,"王五":12,"赵六":13}
    ```

    ```java
    // 分组
    List<User> list = Arrays.asList(new User("张三", 11), new User("李四", 12), new User("王五", 11), new User("赵六", 123));
    Map<Integer, List<User>> result = list.stream().collect(Collectors.groupingBy(User::getAge));
    System.out.println(JsonUtils.objectToJson(result)); // {"123":[{"name":"赵六","age":123}],"11":[{"name":"张三","age":11},{"name":"王五","age":11}],"12":[{"name":"李四","age":12}]}
    ```

    ```java
    // 连接
    List<User> list = Arrays.asList(new User("张三", 11), new User("李四", 12), new User("王五", 11), new User("赵六", 123));
    String result = list.stream().map(User::getName).collect(Collectors.joining("', '", "'", "'"));
    System.out.println(JsonUtils.objectToJson(result)); // "'张三', '李四', '王五', '赵六'"
    ```

    

#### 短路:

- 获取第一个元素:

  - findFirst:

    ```java
    List<User> list = Arrays.asList(new User("张三", 11), new User("李四", 12), new User("王五", 11), new User("赵六", 123));
    User result = list.stream().findFirst().orElse(new User("", 0));
    System.out.println(JsonUtils.objectToJson(result)); // {"name":"张三","age":11}
    ```

- 获取任意元素:

  - findAny:

    ```java
    List<User> list = Arrays.asList(new User("张三", 11), new User("李四", 12), new User("王五", 11), new User("赵六", 123));
    User result = list.parallelStream().findAny().orElse(null);
    System.out.println(JsonUtils.objectToJson(result)); // {"name":"王五","age":11}
    ```

  