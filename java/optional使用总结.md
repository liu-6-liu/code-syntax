### Optional容器:

---

- 静态方法: 

  - Optional.ofNullable(): 将指定的值放入容器, 可以为null

  - Optional.of(): 将指定的值放入容器, 不以为null, 为null则报错

  - Optional.empty(): 获取一个空的容器

- 实例方法:
  - optional.orElse(): 如果容器内数据为null则返回此方法中的值
  - optional.isPresent(): 容器内数据是否有值
  - optional.get(): 获取容器内数据
  - optional.ifPresent(Consumer): 如果容器内数据不为空则执行的操作